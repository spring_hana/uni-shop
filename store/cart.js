export default {
	//开启命名空间
	namespaced: true,
	state: () => ({
		//购物车的数组，用来存储购物车中每个商品的信息对象
		//每个商品的信息对象，都包含如下6个属性
		//{goods_id,goods_neme,goods_price,goods_count,goods_small_logo,goods_state}
		cart: JSON.parse(uni.getStorageSync('cart') || '[]')
	}),
	//更改state
	mutations: {
		//向购物车添加
		addToCart: (state, goods) => {
			//根据提交的商品id查询购物车是否存在这件商品
			//如果不存在findResult为undefined,否则为查找到的商品商品信息对象
			const findResult = state.cart.find(item => item.goods_id === goods.goods_id)
			if (!findResult) {
				//如果购物车没有这件商品直接添加
				state.cart.push(goods)
			} else {
				//如果有直接增加数量
				findResult.goods_count++
			}
			// this.commit('m_cart/saveToStorage')			
			uni.setStorageSync('cart', JSON.stringify(state.cart))
		},
		//将购物车数据存储到本地
		saveToStorage(state) {
			uni.setStorageSync('cart', JSON.stringify(state.cart))
		},
		//修改购物车商品勾选状态
		updateGoodsState(state, goods) {
			//查找对应商品
			const findResult = state.cart.find(item => item.goods_id === goods.goods_id)
			//修改对应商品勾选状态
			if (findResult) {
				findResult.goods_state = goods.goods_state
				//持久化存储到本地
				uni.setStorageSync('cart', JSON.stringify(state.cart))
			}
		},
		//监听修改购物车商品数量
		updateGoodsCount(state, goods) {
			const findResult = state.cart.find(item => item.goods_id === goods.goods_id)
			if (findResult) {
				findResult.goods_count = goods.goods_count
				uni.setStorageSync('cart', JSON.stringify(state.cart))
			}
		},
		//商品右滑删除
		removeGoodsById(state, goods_id) {
			//filter过滤，将除了所传id对应的商品重新赋值给state.cart
			state.cart = state.cart.filter(item => item.goods_id !== goods_id)
			uni.setStorageSync('cart', JSON.stringify(state.cart))
		},
		//更新所有商品勾选状态
		updateAllGoodsState(state,newState){
			//循环更新购物车中商品勾选状态
			state.cart.forEach((item)=>item.goods_state=newState)
			//持久化存储到本地
			uni.setStorageSync('cart',JSON.stringify(state.cart))
		}
	},
	//获取计算后的state(getters中的方法不需要写())
	getters: {
		//统计购物车商品总数
		total(state) {
			// let totalNum = 0
			// state.cart.forEach(goods => totalNum = totalNum + goods.goods_count)
			// return totalNum
			//用reduce方法替代上面代码
			return state.cart.reduce((total,item)=>total+=item.goods_count,0)
		},
		//勾选商品总数量
		checkedCount(state) {
			//reduce循环遍历累加(total:初始值为0,是一个回调参数,i:每一次循环的那一项的数量)
			return state.cart.filter(item => item.goods_state).reduce((total, i) =>total += i.goods_count, 0)
		},
		//计算已勾选商品总价格
		checkedGoodsAmount(state){
			return state.cart.filter(item=>item.goods_state).reduce((price,i)=>price+=i.goods_count*i.goods_price,0).toFixed(2)
		}
	},
	actions: {}
}
