export default{
	//开启命名空间
	namespaced:true,
	state:()=>({
		//读取本地收货地址数据，初始化address
		address: JSON.parse(uni.getStorageSync('address')||'{}')
	}),
	mutations:{
		//更新收货地址方法
		updateAddress(state,address){
			state.address = address
			//将地址持续化存储到本地
			uni.setStorageSync('address',JSON.stringify(state.address))
		}
	},
	getters:{
		//计算详细地址
		addstr(state){
			if(state.address.provinceName) {
				return state.address.provinceName+state.address.cityName+state.address.countyName+state.address.detailInfo
			}else {
				return ''
			}
		}
	}
}