//导入vue和vuex
import Vue from "vue"
import Vuex from "vuex"
//导入购物车的vuex模块
import cart from "@/store/cart.js"
//导入收货地址的vuex模块
import user from "@/store/user.js"

//将vuex安装为vue的插件
Vue.use(Vuex)

//全局状态数据
// const state = {
	// a:"GG"
// }
//方法
// const mutations = {
	//第一个参数都是state
	// LOGIN:(state,val)=>{
	// 	state.a=val
	// }
// }
// const getters = {}
// const actions = {}
	//挂载store模块
// const modules = {
	//挂载购物车的vuex模块
	// 'm_cart':cart
// }
//创建store实例对象
const store = new Vuex.Store({
	//全局状态数据(this.$store.state.a)
	state:{
	},
	//同步(this.$store.commit('addToCart',val))方法
	mutations:{
		
	},
	//函数
	getters:{},
	//异步
	actions:{},
	//挂载store模块
	modules:{
		//挂载购物车的vuex模块
		'm_cart':cart,
		'm_user':user
	}
})
export default store