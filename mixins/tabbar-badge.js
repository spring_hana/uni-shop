import { mapGetters } from 'vuex'
export default {
	computed: {
		...mapGetters('m_cart', ['total'])
	},
	//监听商品数量变化，使购物车数标跟随改变
	watch:{
		total:{
			handler(newVal){
				this.setBadge()
			}
		}
	},
	onShow() {
		this.setBadge()
	},
	methods: {
		setBadge(){
			//setTabBarBadge为 tabBar 某一项的右上角添加文本
			uni.setTabBarBadge({
				index:2,  //tabBar索引
				text:this.total+'' //文本内容(必须是字符串，不能是数字)
			})
		}
	}
}